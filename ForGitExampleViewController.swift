//
//  ForGitExampleViewController.swift
//  apirest
//
//  Created by Manam on 12/10/22.
//

import UIKit

class ForGitExampleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.sizeToFit()
        view.layer.masksToBounds = true
        
        //to try smallchanges so that to merge with main
        view.clipsToBounds = true
        view.layer.cornerRadius = 44
        view.isOpaque = true
        view.isUserInteractionEnabled = true
        //--------------------\\

    }
    

  

}
