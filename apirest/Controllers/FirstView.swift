//
//  FirstView.swift
//  apirest
//
//  Created by Manam on 11/03/22.
//

import UIKit

class FirstView: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate{

    @IBOutlet var nextButton: UIButton!
    @IBOutlet var mobilenumber: UITextField!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet var mobileView: UIView!
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*
         to test theemergency fix we are doing this for git lab by creating
         - a new branch
         - seperated from both the main and new branches
         - used to fix errors
         
         
         
         */
        
        
        
        
        
        
        
        
        mobileView.layer.cornerRadius=10
        
        self.Sample()

       
    }
    
    
    @IBAction func nextView(_ sender: Any) {

        
        if mobilenumber.text!.count < 10 {
            // alert enter a valid mob no
            let alertController=UIAlertController(title: "Wrong Number", message: "The  number must contain 10 characters", preferredStyle: .alert)
            let Cancelaction=UIAlertAction(title: "Close", style: .cancel){ _ in
                Swift.debugPrint("alert has occured")
            }
            alertController.addAction(Cancelaction)
            present(alertController, animated: true, completion: nil)
            
        }else {
            
     // move to next screen
            
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let secondview = story.instantiateViewController(withIdentifier: "SecondView") as? SecondView
            secondview?.mobileNumber = mobilenumber.text
            self.navigationController?.pushViewController(secondview!, animated: true)
           
      
        }
    
    }
    
   
    
func Sample()
    {
        
        self.nextButton.layer.cornerRadius = self.nextButton.frame.size.height/2
        self.nextButton.clipsToBounds = true
//        self.nextButton.frame.size.height/2
       
        
    }
   
    

}
    


