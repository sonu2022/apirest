//
//  SecondView.swift
//  apirest
//
//  Created by Manam on 11/03/22.
//

import UIKit

class SecondView: UIViewController, SelectClassDelegare, SelectPackageDelegate {
    
    
    
    @IBOutlet var loader: UIActivityIndicatorView!
    
    @IBOutlet var classTextField: UITextField!
    
    @IBOutlet var packageTextField: UITextField!
    var mobileNumber: String?
    
    @IBOutlet var number_label: UILabel!
    @IBOutlet var amountTextField: UITextField!
    
    // @IBOutlet var amountButton: UIButton!
    @IBOutlet var packageButton: UIButton!
    @IBOutlet var classButton: UIButton!
    
    let apiClass = ApiClass()
    var session_id : Int?
    var classesArray:[Classes] = []
    var class_id : String?
    var packageArray:[Packages] = []
    var packageId : String?
    var numberOfDays : String?
    var rateArray:[Rate]=[]
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        number_label.text = mobileNumber
        self.loader.isHidden = false
        self.loader.startAnimating()
        self.callApiOne()

        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func selectClass(_ sender: Any) {
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let showClassesView = story.instantiateViewController(withIdentifier: "ShowClassesView") as? ShowClassesView
        showClassesView?.classesArray = classesArray
        showClassesView?.delegate = self
        self.present(showClassesView!, animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func selectPackage(_ sender: Any) {

        if session_id != nil && class_id != nil {
            self.loader.isHidden = false
            self.loader.startAnimating()
            self.callApiTwo()
            
        }else{
            
            // alert please select class
            let alertController=UIAlertController(title: "Blank Error", message: "Please select a value from the class", preferredStyle: .alert)
            let Cancelaction=UIAlertAction(title: "Close", style: .cancel){ _ in
                Swift.debugPrint("alert has occured")
            }
            alertController.addAction(Cancelaction)
                    
            
            present(alertController, animated: true, completion: nil)
        }

        
    }
    
    
    
    
    
    func selectedPackageDetails(packageId: String, packageName: String, numberOfDays: String) {
        print("packageName =", packageName)
        //self.packageButton.setTitle(packageName, for: .normal)
//        self.packageId=packageId
//        self.numberOfDays=numberOfDays
        self.packageTextField.text=packageName
        self.callApiThree(number_of_days: numberOfDays, package_id: packageId, session_id: self.session_id!)
        
        
    }
    
    
    
    
    
    
    
    func selectedClassDetails(className: String, class_id: String) {
        self.classTextField.text = className
        //self.classButton.setTitle(className, for: .normal)
        self.class_id = class_id
        
    }
   
    
    func callApiOne() {
        
        let now = Date()

            let formatter = DateFormatter()

            formatter.timeZone = TimeZone.current

            formatter.dateFormat = "ddMMyyyyHHmmssSSS"

            let dateString = formatter.string(from: now)
        
        let refID = "256" + mobileNumber! + dateString
        print(refID)
        
        apiClass.getRequest(url: "https://uatservices.khalti.com/api/servicegroup/search/midas/", token: "TEST:FABBAWfHpfLmBSiKEp7h", number: "9803595804", reference: refID, comp: { class_list in
            self.classesArray = class_list.classes!
            self.session_id = class_list.session_id
           
            

            DispatchQueue.main.async {
                self.loader.stopAnimating()
                self.loader.isHidden = true
                print("classesArray = ", self.classesArray)
            }
            
            
            
        })
        
    }
    
    
    func callApiTwo(){
        
        
        apiClass.postApiMethod(class_id: self.class_id!, session_id: self.session_id!, token: "TEST:FABBAWfHpfLmBSiKEp7h", comp: { packageClass in
            
            
            print("packageClass", packageClass)
            self.packageArray = packageClass.packages!
            
            if packageClass.packages!.count > 0 {
                DispatchQueue.main.async {
                    self.loader.isHidden = true
                    self.loader.stopAnimating()
                    //it has a storyboard
                    let story=UIStoryboard.init(name: "Main", bundle: nil)
                    let showPackageView=story.instantiateViewController(withIdentifier: "ShowPackageView") as? ShowPackageView
                    showPackageView?.packageArray=self.packageArray
                    showPackageView?.delegate = self
                    self.present(showPackageView!, animated: true, completion: nil)
                }
                
                
            }
            
           
            
            
            
        })
        
        
    }

    
    func callApiThree(number_of_days: String,package_id: String,session_id:Int){
        
        
        
        apiClass.postApiMethodAmount(number_of_days: number_of_days, package_id: package_id, session_id: session_id, token: "TEST:FABBAWfHpfLmBSiKEp7h", comp: { rateClass in
           // print("RateClass",rateClass.rate?.amount! as Any)

            DispatchQueue.main.async {

                self.amountTextField.text = rateClass.rate?.amount
                

            }
    
    
    
})
}
    
    
}
