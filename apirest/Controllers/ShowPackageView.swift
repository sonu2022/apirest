//
//  ShowPackageView.swift
//  apirest
//
//  Created by Manam on 15/03/22.
//

import UIKit

@objc protocol SelectPackageDelegate{
    func selectedPackageDetails(packageId:String,packageName:String,numberOfDays:String)
}

class ShowPackageView: UIViewController,UISearchBarDelegate {
    
    @IBOutlet var packageSearchBar: UISearchBar!
    @IBOutlet var package_table: UITableView!
     var packageArray:[Packages] = []
    
    var filteredArray:[Packages]=[]
    
    var isSearching: Bool? = false
    
    @objc var delegate:SelectPackageDelegate? = nil
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        packageSearchBar.delegate=self
        packageSearchBar.showsCancelButton = true
        filteredArray=packageArray
    
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }

    func searchBar(searchBar:UISearchBar, textDidchange searchText:String){
        print("searchText\(searchText)")
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing:searchBar.text))")
       }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText:String){
        print("searchText==",searchText)
        filteredArray = packageArray.filter({$0.package_name?.lowercased().contains(searchText.lowercased()) as! Bool })
        self.isSearching=true
        package_table.reloadData()
        if searchText == "" || searchText.count == 0{
            self.isSearching=false
            package_table.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching=false
        searchBar.text=""
        package_table.reloadData()
    }
    
    
}
extension ShowPackageView:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching! {
            return filteredArray.count
            
        }else{
       return self.packageArray.count
    }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell1")
        if cell1 == nil{
            cell1 = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell1")
        }
        if isSearching! {
            cell1?.textLabel?.text = filteredArray[indexPath.row].package_name
        }else{
        cell1?.textLabel?.text=packageArray[indexPath.row].package_name
        }
        return cell1!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching! {
            let packageName = filteredArray[indexPath.row
            ].package_name ?? ""
            let packageId = filteredArray[indexPath.row].package_id ?? ""
            let numberOfDays=filteredArray[indexPath.row].number_of_days ?? ""
            self.delegate?.selectedPackageDetails(packageId: packageId, packageName: packageName, numberOfDays: numberOfDays)
        }else{
        let packageName=packageArray[indexPath.row
        ].package_name ?? ""
        let packageId = packageArray[indexPath.row].package_id ?? ""
        let numberOfDays=packageArray[indexPath.row].number_of_days ?? ""
            self.delegate?.selectedPackageDetails(packageId: packageId, packageName: packageName, numberOfDays: numberOfDays)}
        self.dismiss(animated: true, completion: nil)
    
    }
    
}
