//
//  ShowClassesView.swift
//  apirest
//
//  Created by Manam on 11/03/22.
//

import UIKit

@objc protocol SelectClassDelegare {
    
    func selectedClassDetails(className: String, class_id: String)
    
    
}



class ShowClassesView: UIViewController,UISearchBarDelegate {
    

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var class_table: UITableView!
    
    var classesArray:[Classes] = []
    var filteredArray:[Classes]=[]
    
    var isSearching: Bool? = false
    
    @objc var delegate:SelectClassDelegare? = nil
        override func viewDidLoad() {
        super.viewDidLoad()
            
        searchBar.delegate=self
        searchBar.showsCancelButton = true
        filteredArray=classesArray
            
        
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
  
   func searchBar(searchBar:UISearchBar, textDidchange searchText:String){
        print("searchText\(searchText)")
    }
        
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing: searchBar.text))")
       }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText == ",searchText)
        filteredArray = classesArray.filter({$0.class_name?.lowercased().contains(searchText.lowercased()) as! Bool })
        self.isSearching = true
        class_table.reloadData()
        if searchText == "" || searchText.count == 0 { // rmoving filter when text is empty on search bar
            self.isSearching = false
            class_table.reloadData()
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // rmoving filter when cancel button on search bar
        self.isSearching = false
        searchBar.text = ""
        class_table.reloadData()
    }
            

}

extension ShowClassesView: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching! {
            return filteredArray.count
        }else{
            return self.classesArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")

            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }

        if isSearching! {
            cell?.textLabel?.text = filteredArray[indexPath.row].class_name
        }else{
            cell?.textLabel?.text = classesArray[indexPath.row].class_name
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearching! {
        let className = filteredArray[indexPath.row].class_name ?? ""
        let class_id = filteredArray[indexPath.row].class_id ?? ""
        self.delegate?.selectedClassDetails(className: className, class_id: class_id)
        }else{
            let className = classesArray[indexPath.row].class_name ?? ""
            let class_id = classesArray[indexPath.row].class_id ?? ""
            self.delegate?.selectedClassDetails(className: className, class_id: class_id)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}
