//
//  ApiClass.swift
//  apirest
//
//  Created by Manam on 10/03/22.
//

import Foundation

class ApiClass:NSObject {

    func getRequest(url: String, token: String, number: String, reference:String, comp: @escaping(BaseClass)->()) {
        
        
        let params = NSString(format: "%@=%@&%@=%@&%@=%@", "token",token,"number",number,"reference",reference)
        let urlString = NSString(format: "%@?%@", url,params)
        let url = NSURL(string: urlString as String)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
            

            if error != nil{
                print("Error -> \(String(describing: error))")
            }else{
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        print(json)
  
                        let baseclass = BaseClass(dictionary: json as NSDictionary)
                        
                        comp(baseclass!)

                       }
                    
                    
                } catch {
                    print("Error -> \(error)")
                }
                }
            }
                
            task.resume()
        
        
        
    }
    
    func postApiMethod(class_id:String, session_id:Int, token: String,comp: @escaping(PackageClass)->()){
        guard let url=URL(string: "https://uatservices.khalti.com/api/servicegroup/getpackages/midas/") else{
            print("cannot getting the API")
            return
        }
        struct uploadData:Codable{
            //we are creating the model
            let class_id:String
            let session_id:Int
            let token:String
        }
        //we need to fetch data to this model
        let uploadDataModel=uploadData(class_id: class_id, session_id: session_id, token:token)
        //covert the model the json model
        guard let jsonData=try?JSONEncoder().encode(uploadDataModel) else{
            print("ERROR: trting to convert the datamode")
            return
        }
        //we are creating the url request
        var request=URLRequest(url: url)
        request.httpMethod="POST"
        //the request is JSON
        request.setValue("application/json", forHTTPHeaderField: "content-type")
            //the response expected to be in JSON
        request.setValue("application/json", forHTTPHeaderField: "accept")
        request.httpBody=jsonData
     let task=URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else{
                print("Error calling the POST")
                print(error!)
                return
            }
            guard let data = data else {
                print("Did not recieve the data")
                return
            }
         do {
             
             if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                 print(json)

                 let packageList = PackageClass(dictionary: json as NSDictionary)

                 
                 comp(packageList!)

                }
             
             
         } catch {
             print("Error -> \(error)")
         }
         
         
         
           
     }
        task.resume()
       
    }
    
    func postApiMethodAmount(number_of_days:String,package_id:String,session_id:Int,token:String, comp: @escaping(RateBase)->()){
        guard let url = URL(string: "https://uatservices.khalti.com/api/servicegroup/getpackagerates/midas/")else{
            print("Not getting the API")
            return
        }
        struct getAmount:Codable{
            //creating the model
            let number_of_days:String
            let package_id:String
            let session_id:Int
            let token:String
        }
       let getAmountModel=getAmount(number_of_days: number_of_days, package_id: package_id, session_id: session_id, token: token)
        //converting json model
        guard let jsonData=try?JSONEncoder().encode(getAmountModel) else{
            print("ERROR: while trying to convert to model")
            return
        }
        //create url request
        var request=URLRequest(url: url)
        request.httpMethod="POST"
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        request.setValue("application/json", forHTTPHeaderField: "accept")
        request.httpBody=jsonData
        let task1=URLSession.shared.dataTask(with: request) { data, response, error in
            guard error==nil else{
                print("Error while calling the post")
                print(error!)
                return
            }
            guard let data=data else{
                print("Did not recieve the data")
                return
            }
          
            do{
                if let json = try
                    JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                        print(json)
                    
                    let amountValue = RateBase(dictionary: json as NSDictionary)
                    comp(amountValue!)
                    }
                }catch{
                    print("Error ->\(error)")
                }
    }
                          
                          task1.resume()
    
    
}
                                
}
/*


*/
