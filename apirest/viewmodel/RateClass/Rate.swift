/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Rate {
	public var package_id : String?
	public var package_code : String?
	public var number_of_days : String?
	public var amount : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let rate_list = Rate.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Rate Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Rate]
    {
        var models:[Rate] = []
        for item in array
        {
            models.append(Rate(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let rate = Rate(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Rate Instance.
*/
	required public init?(dictionary: NSDictionary) {

		package_id = dictionary["package_id"] as? String
		package_code = dictionary["package_code"] as? String
		number_of_days = dictionary["number_of_days"] as? String
		amount = dictionary["amount"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.package_id, forKey: "package_id")
		dictionary.setValue(self.package_code, forKey: "package_code")
		dictionary.setValue(self.number_of_days, forKey: "number_of_days")
		dictionary.setValue(self.amount, forKey: "amount")

		return dictionary
	}

}